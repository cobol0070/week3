       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA3.
       AUTHOR. WIMONSIRI.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SURNAME           PIC   X(8)   VALUE "COUGHLAN".
       01  SALE-PICE         PIC   9(4)V99.
       01  NUM-OF-EMPLOYEES  PIC 999V99.
       01  SALARY            PIC 9999V99.
       01  COUNTY-NAME       PIC X(9).
       PROCEDURE DIVISION.
       Begin.
           DISPLAY "1 " SURNAME 
           MOVE "SMITH" TO SURNAME  
           DISPLAY "2 " SURNAME
           MOVE "FITZWILLIAM" TO SURNAME
           DISPLAY "3 " SURNAME
           .
           DISPLAY "1 " SALE-PICE 
           MOVE ZEROES TO SALE-PICE
           DISPLAY "2 " SALE-PICE 
           MOVE 25.5 TO SALE-PICE
           DISPLAY "3 " SALE-PICE 
      * 01  SALE-PICE       PIC   9(4)V99. 0007.55
           MOVE 7.553 TO SALE-PICE
           DISPLAY "4 " SALE-PICE 
      * 01  SALE-PICE       PIC   9(4)V99. 3425.15
           MOVE 93425.158 TO SALE-PICE
           DISPLAY "5 " SALE-PICE      
      * 01  SALE-PICE       PIC   9(4)V99. 0128.00
           MOVE 128 TO SALE-PICE
           DISPLAY "6 " SALE-PICE 
           .

      *01  NUM-OF-EMPLOYEES  PIC 999V99.
           DISPLAY NUM-OF-EMPLOYEES 
      *01  NUM-OF-EMPLOYEES  PIC 999V99. 012.40
           MOVE 12.4 TO NUM-OF-EMPLOYEES
           DISPLAY NUM-OF-EMPLOYEES 
      *01  NUM-OF-EMPLOYEES  PIC 999V99.  745.00
           MOVE 6745 TO NUM-OF-EMPLOYEES
           DISPLAY NUM-OF-EMPLOYEES .

      *01  SALARY            PIC 9999V99.    0745.00 
           MOVE NUM-OF-EMPLOYEES TO SALARY
           DISPLAY SALARY 
           .

      *01  COUNTY-NAME       PIC X(9).
           MOVE "GALWAY" TO COUNTY-NAME
           DISPLAY COUNTY-NAME 
           MOVE ALL "WI_" TO COUNTY-NAME 
           DISPLAY COUNTY-NAME 
           .


